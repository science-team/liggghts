Source: liggghts
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Anton Gladky <gladk@debian.org>
Section: science
Priority: optional
Build-Depends: dpkg-dev (>= 1.22.5),
               cmake,
               debhelper-compat (= 13),
               libboost-mpi-dev,
               libeigen3-dev,
               libjpeg-dev,
               libvtk9-dev,
               mpi-default-bin,
               mpi-default-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/science-team/liggghts
Vcs-Git: https://salsa.debian.org/science-team/liggghts.git
Homepage: https://www.liggghts.com/
Rules-Requires-Root: no

Package: libliggghts-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libliggghts3t64 (= ${binary:Version}),
         mpi-default-bin,
         ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Open Source DEM Particle Simulation Software. Development files
 LIGGGHTS stands for LAMMPS improved for general granular and granular
 heat transfer simulations.
 .
 LAMMPS is a classical molecular dynamics simulator. It is widely used in
 the field of Molecular Dynamics. Thanks to physical and algorithmic analogies,
 LAMMPS is a very good platform for DEM simulations. LAMMPS offers a GRANULAR
 package to perform these kind of simulations. LIGGGHTS aims to improve those
 capability with the goal to apply it to industrial applications.
 Development version.
 .
 The package contains development files.

Package: libliggghts3t64
Architecture: any
Multi-Arch: same
Depends: mpi-default-bin,
         ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Breaks: libliggghts3 (<< ${source:Version})
Provides: ${t64:Provides}
Replaces: libliggghts3
Description: Open Source DEM Particle Simulation Software. Shared library
 LIGGGHTS stands for LAMMPS improved for general granular and granular
 heat transfer simulations.
 .
 LAMMPS is a classical molecular dynamics simulator. It is widely used in
 the field of Molecular Dynamics. Thanks to physical and algorithmic analogies,
 LAMMPS is a very good platform for DEM simulations. LAMMPS offers a GRANULAR
 package to perform these kind of simulations. LIGGGHTS aims to improve those
 capability with the goal to apply it to industrial applications.
 Development version.
 .
 The package contains shared library.

Package: liggghts
Architecture: any
Multi-Arch: foreign
Depends: libliggghts3t64 (>= ${source:Version}),
         mpi-default-bin,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: paraview
Description: Open Source DEM Particle Simulation Software.
 LIGGGHTS stands for LAMMPS improved for general granular and granular
 heat transfer simulations.
 .
 LAMMPS is a classical molecular dynamics simulator. It is widely used in
 the field of Molecular Dynamics. Thanks to physical and algorithmic analogies,
 LAMMPS is a very good platform for DEM simulations. LAMMPS offers a GRANULAR
 package to perform these kind of simulations. LIGGGHTS aims to improve those
 capability with the goal to apply it to industrial applications.
 Development version.

Package: liggghts-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libjs-jquery,
         libjs-underscore
Recommends: liggghts
Description: Open Source DEM Particle Simulation Software. Documentation and examples
 LIGGGHTS stands for LAMMPS improved for general granular and granular
 heat transfer simulations.
 .
 LAMMPS is a classical molecular dynamics simulator. It is widely used in
 the field of Molecular Dynamics. Thanks to physical and algorithmic analogies,
 LAMMPS is a very good platform for DEM simulations. LAMMPS offers a GRANULAR
 package to perform these kind of simulations. LIGGGHTS aims to improve those
 capability with the goal to apply it to industrial applications.
 Development version.
 .
 The package contains documentation and examples.
