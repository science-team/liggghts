liggghts (3.8.0+repack1-13) unstable; urgency=medium

  * [60d1da6] Remove heat test. (Closes: #1091874)
  * [653ff6b] Suppress false-positive lintian errors

 -- Anton Gladky <gladk@debian.org>  Sat, 01 Mar 2025 13:12:48 +0100

liggghts (3.8.0+repack1-12) unstable; urgency=medium

  * [c2406e8] Remove unsupported mpirun parameters

 -- Anton Gladky <gladk@debian.org>  Mon, 16 Dec 2024 19:16:51 +0100

liggghts (3.8.0+repack1-11) unstable; urgency=medium

  * [678093a] d/control: set Standards-Version to 4.7.0
  * [e5c1e59] Remove deprecated mpirun parameter. (Closes: #1076233)
  * [4e789b6] Make the package reproducible. (Closes: #1006979)

 -- Anton Gladky <gladk@debian.org>  Sat, 14 Dec 2024 18:54:53 +0100

liggghts (3.8.0+repack1-10) unstable; urgency=medium

  * [7ef3764] d/control: set Standards-Version to 4.6.2
  * [d71f41f] Update watch file format version to 4.
  * [38e52a2] Use secure URI in Homepage field.
  * [fbafb15] Fix FTBFS with VTK_9.3.0. (Closds: #1072823)

 -- Anton Gladky <gladk@debian.org>  Sun, 09 Jun 2024 09:33:27 +0200

liggghts (3.8.0+repack1-9.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062745

 -- Steve Langasek <vorlon@debian.org>  Fri, 01 Mar 2024 07:30:31 +0000

liggghts (3.8.0+repack1-9) unstable; urgency=medium

  * [67ea6b6] Fix FTBFS with GCC-11. (Closes: #984217)

 -- Anton Gladky <gladk@debian.org>  Tue, 19 Oct 2021 20:49:53 +0200

liggghts (3.8.0+repack1-8) unstable; urgency=medium

  * [5aa8084] Update .gitlab-ci.yml
  * [ba569b0] Switch to vtk9
  * [d5b17fb] Bump debhelper from old 12 to 13.
    + Drop check for DEB_BUILD_OPTIONS containing "nocheck",
    since debhelper now does this.
  * [a5fd6a3] Avoid explicitly specifying -Wl,--as-needed linker flag.
  * [3621eaf] Set Standards-Version 4.6.0

 -- Anton Gladky <gladk@debian.org>  Sat, 25 Sep 2021 14:47:58 +0200

liggghts (3.8.0+repack1-7) unstable; urgency=medium

  [ Anton Gladky ]
  * [984ea96] Let reprotest fail
  * [67f38aa] Add Rules-Requires-Root: no

  [ Helmut Grohne ]
  * [b510414] Support DEB_BUILD_OPTIONS=nocheck. (Closes: #943310)
  * [fd665f2] Do not use not use MPI_C_COMPILER or MPI_CXX_COMPILER
    (Closes: #943313)

 -- Anton Gladky <gladk@debian.org>  Fri, 26 Jun 2020 23:24:34 +0200

liggghts (3.8.0+repack1-6) unstable; urgency=medium

  * [db8d4d8] Trim trailing whitespace.
  * [e5d9a66] Bump debhelper from old 11 to 12.
  * [1ce0a3d] Set debhelper-compat version in Build-Depends.
  * [249f192] Update standards version to 4.5.0, no changes needed.
  * [379ddc7] Fix manpage. (Closes: #963449)
  * [bbccc13] Add debian/.gitlab-ci.yml

 -- Anton Gladky <gladk@debian.org>  Thu, 25 Jun 2020 23:16:30 +0200

liggghts (3.8.0+repack1-5) unstable; urgency=medium

  * Team upload.
  * Drop unused libpython-dev dependency (Closes: #936944).

 -- Stuart Prescott <stuart@debian.org>  Tue, 21 Jan 2020 13:11:38 +1100

liggghts (3.8.0+repack1-4) unstable; urgency=medium

  * [b42dedf] Update CMakeListst. (Closes: 917500)

 -- Anton Gladky <gladk@debian.org>  Tue, 22 Jan 2019 20:40:32 +0100

liggghts (3.8.0+repack1-3) unstable; urgency=medium

  * [5cb8750] Set LC_ALL=C in d/rules. (Closes: #917500)

 -- Anton Gladky <gladk@debian.org>  Mon, 21 Jan 2019 23:56:07 +0100

liggghts (3.8.0+repack1-2) unstable; urgency=medium

  * [8bc3eb8] Remove qt4-framework from deps. (Closes: #875028)
  * [5ae1881] Update standards-version to 4.3.0

 -- Anton Gladky <gladk@debian.org>  Sat, 19 Jan 2019 15:42:41 +0100

liggghts (3.8.0+repack1-1) unstable; urgency=medium

  * [77bb37d] Update patches
  * [9c9888d] Apply cme fix dpkg
  * [2a69e72] Add allow-run-as-root option
  * [a0fb4a3] Remove unneeded fields in d/control and d/rules
  * [211b0af] Use packaged version of the js-libs
  * [4ecc15e] Fix typo in d/changelog
  * [b9e4366] Update d/watch
  * [15fd630] New upstream version 3.8.0+repack1

 -- Anton Gladky <gladk@debian.org>  Sun, 23 Dec 2018 20:53:10 +0100

liggghts (3.7.0+repack1-1) unstable; urgency=medium

  * [56c1d27] Drop windows-files from the source.
  * [a769922] Filter some js-files from the source.
  * [e042cd2] New upstream version 3.7.0+repack1
  * [d14defe] Refresh patches, remove unneeded ones.
  * [8812f5f] Set maximal hardening level.

 -- Anton Gladky <gladk@debian.org>  Tue, 18 Jul 2017 00:15:56 +0200

liggghts (3.5.0+repack1-10) unstable; urgency=medium

  * [e2a6253] Better fix for the bug, fixed in previous upload.
              Thanks, Graham

 -- Anton Gladky <gladk@debian.org>  Thu, 19 Jan 2017 18:58:57 +0100

liggghts (3.5.0+repack1-9) unstable; urgency=medium

  * [a879ac6] Do not fail on mpi-errors. (Closes: #851726)

 -- Anton Gladky <gladk@debian.org>  Wed, 18 Jan 2017 22:46:29 +0100

liggghts (3.5.0+repack1-8) unstable; urgency=medium

  * [2219bae] Fix variable name in d/rules.

 -- Anton Gladky <gladk@debian.org>  Fri, 30 Dec 2016 08:26:31 +0100

liggghts (3.5.0+repack1-7) unstable; urgency=medium

  * [6079250] Disable test on mips64el (unstable).

 -- Anton Gladky <gladk@debian.org>  Fri, 30 Dec 2016 00:24:51 +0100

liggghts (3.5.0+repack1-6) unstable; urgency=medium

  * [96fa01f] Add mips64el to disable_auto_test_archs_mpi in d/rules.

 -- Anton Gladky <gladk@debian.org>  Thu, 29 Dec 2016 20:02:34 +0100

liggghts (3.5.0+repack1-5) unstable; urgency=medium

  [ Graham Inggs ]
  * [fe1b4d7] Replace OMPI_MCA_orte_rsh_agent by OMPI_MCA_plm_rsh_agent
              for autopkgtests too
  * [5037eeb] Allow --oversubscribe for autopkgtests

 -- Anton Gladky <gladk@debian.org>  Wed, 28 Dec 2016 19:01:44 +0100

liggghts (3.5.0+repack1-4) unstable; urgency=medium

  [ Graham Inggs ]
  * Restrictions: allow-stderr on the tests, for compatibility with current
    openmpi.
  * Update autopkgtests with examples from 3.5.0.

  [ Anton Gladky ]
  * Use --parallel build again

 -- Anton Gladky <gladk@debian.org>  Tue, 08 Nov 2016 20:25:41 +0100

liggghts (3.5.0+repack1-2) unstable; urgency=medium

  * [8031b0e] Do not use parallel build. Fails on some platforms.
  * [6a2a7ed] Fix autotests.

 -- Anton Gladky <gladk@debian.org>  Sat, 05 Nov 2016 15:46:43 +0100

liggghts (3.5.0+repack1-1) unstable; urgency=medium

  * [784df99] New upstream version 3.5.0+repack1
  * [4101537] Refresh patches.

 -- Anton Gladky <gladk@debian.org>  Thu, 27 Oct 2016 23:04:19 +0200

liggghts (3.4.0+repack1-2) unstable; urgency=medium

  * [ead3d70] Disable mpi-test on powerpc.
  * [6492a8e] Apply cme fix dpkg.

 -- Anton Gladky <gladk@debian.org>  Wed, 27 Jul 2016 00:44:20 +0200

liggghts (3.4.0+repack1-1) unstable; urgency=medium

  * [6a5c2a8] Imported Upstream version 3.4.0+repack1
  * [157da23] Refresh patches.
  * [9a184e0] Apply cme fix dpkg.
  * [ea651a2] Update d/copyright.

 -- Anton Gladky <gladk@debian.org>  Wed, 15 Jun 2016 22:12:40 +0200

liggghts (3.3.1+repack1-1) unstable; urgency=medium

  * [3697863] Imported Upstream version 3.3.1+repack1
  * [662e977] Refresh patches.
  * [df1a7e8] Remove dbg-file.

 -- Anton Gladky <gladk@debian.org>  Sun, 17 Jan 2016 00:55:04 +0100

liggghts (3.2.0+repack1-3) unstable; urgency=medium

  * [62739c0] Add support for VTK6.2. (Closes: #793605)

 -- Anton Gladky <gladk@debian.org>  Tue, 04 Aug 2015 21:14:21 +0200

liggghts (3.2.0+repack1-2) unstable; urgency=medium

  * [7db5a48] Update d/watch.
  * [ba29637] Update d/copyright.
  * [b81a33d] Refresh patches.
  * [9d49744] Imported Upstream version 3.2.0+repack1

 -- Anton Gladky <gladk@debian.org>  Mon, 22 Jun 2015 18:17:22 +0200

liggghts (3.1.0+repack1-1) unstable; urgency=medium

  * [6ac272b] Use cme fix dpkg-control.

 -- Anton Gladky <gladk@debian.org>  Wed, 06 May 2015 22:16:12 +0200

liggghts (3.1.0+repack1-1~exp1) experimental; urgency=medium

  * [4d4603b] Add some more files to exclude in d/copyright.
  * [39f1279] Imported Upstream version 3.1.0+repack1
  * [ea63dab] Update patches.
  * [a0b9219] Enable C++11 for build.
  * [c74e08e] Remove README.source.

 -- Anton Gladky <gladk@debian.org>  Tue, 17 Mar 2015 21:46:02 +0100

liggghts (3.0.3+repack-2) unstable; urgency=medium

  * [57ea03a] Specify components for VTK. (Closes: #765106).
  * [a8da87d] Set Standards-Version: 3.9.6. No changes.

 -- Anton Gladky <gladk@debian.org>  Tue, 14 Oct 2014 22:56:37 +0200

liggghts (3.0.3+repack-1) unstable; urgency=medium

  * [d604745] Remove xdmf from BD.
  * [43935b3] Imported Upstream version 3.0.3+repack

 -- Anton Gladky <gladk@debian.org>  Sun, 07 Sep 2014 20:23:50 +0200

liggghts (3.0.2+repack-1) unstable; urgency=medium

  * [ae36cd8] Add Files-Excluded to copyright-file.
  * [b745737] Exclude LAMMPS-examples.
  * [61a906b] Imported Upstream version 3.0.2+repack
  * [2566925] Set minimal vtk6 version to 6.1.0+dfsg-7.
  * [fb1a0c1] Add libxdmf-dev to BD.
  * [268930b] Remove .gitignore from examples.

 -- Anton Gladky <gladk@debian.org>  Sun, 15 Jun 2014 19:49:34 +0200

liggghts (3.0.1+repack-2) unstable; urgency=medium

  * [84cab80] Fix autopkgtest. (Closes: #747339)

 -- Anton Gladky <gladk@debian.org>  Sat, 17 May 2014 22:53:31 +0200

liggghts (3.0.1+repack-1) unstable; urgency=medium

  * [f333916] Imported Upstream version 3.0.1+repack
  * [ce0d980] Build against VTK6 instead of VTK5.
  * [f8e3515] Add autotest.
  * [833caea] Set Standards-Version: 3.9.5. No changes.

 -- Anton Gladky <gladk@debian.org>  Wed, 23 Apr 2014 22:21:33 +0200

liggghts (3.0.0+repack-1) unstable; urgency=medium

  * [96c3d6e] Imported Upstream version 3.0.0+repack
  * [8bc06d5] Remove lib/ and src/USER-MISC. (Closes: #742026)

 -- Anton Gladky <gladk@debian.org>  Thu, 20 Mar 2014 20:47:28 +0100

liggghts (3.0.0-1) unstable; urgency=medium

  * [76193e3] Imported Upstream version 3.0.0

 -- Anton Gladky <gladk@debian.org>  Tue, 04 Mar 2014 22:23:50 +0100

liggghts (2.3.8-1) unstable; urgency=low

  * [c18713f] Imported Upstream version 2.3.8
  * [8433776] Use wrap-and-sort.

 -- Anton Gladky <gladk@debian.org>  Sun, 10 Nov 2013 20:25:20 +0100

liggghts (2.3.6-1) unstable; urgency=low

  * [e358878] Add debian/watch.
  * [28595e2] Imported Upstream version 2.3.6
  * [a43057c] Ignore quilt dir

 -- Anton Gladky <gladk@debian.org>  Sun, 07 Jul 2013 16:55:39 +0200

liggghts (2.3.2-1) unstable; urgency=low

  * [adf4c2b] Imported Upstream version 2.3.2
  * [8fe04ee] Use cmake for building.
  * [e34c201] Split the package into libliggghts2, liggghts-doc, liggghts2-dbg.

 -- Anton Gladky <gladk@debian.org>  Tue, 07 May 2013 22:47:51 +0200

liggghts (1.5.3-1) unstable; urgency=low

  * [fb49bd6] Imported Upstream version 1.5.3
  * [cbfc470] Fix FTBFS due to gcc-4.7 (missing header).

 -- Anton Gladky <gladky.anton@gmail.com>  Wed, 23 May 2012 22:44:20 +0200

liggghts (1.5.2-3) unstable; urgency=low

  * [b5f1247] Fix wrong defined archs for auto_tests.

 -- Anton Gladky <gladky.anton@gmail.com>  Thu, 15 Mar 2012 21:31:29 +0100

liggghts (1.5.2-2) unstable; urgency=low

  * [e5a0fb3] Disable mpirun-test on some platforms, where it crashes.

 -- Anton Gladky <gladky.anton@gmail.com>  Thu, 15 Mar 2012 20:05:37 +0100

liggghts (1.5.2-1) unstable; urgency=low

  * [5b3ec1c] Imported Upstream version 1.5.2
  * [cfb6bac] Do not start mpirun-autotests on mips* archs.

 -- Anton Gladky <gladky.anton@gmail.com>  Wed, 14 Mar 2012 20:41:52 +0100

liggghts (1.5.1-5) unstable; urgency=low

  * [d7df2ea] Fix typo in archs.

 -- Anton Gladky <gladky.anton@gmail.com>  Sat, 10 Mar 2012 10:10:18 +0100

liggghts (1.5.1-4) unstable; urgency=low

  * [0915b9e] Disable auto-test on arm and armhf, the test hangs.

 -- Anton Gladky <gladky.anton@gmail.com>  Tue, 06 Mar 2012 20:45:00 +0100

liggghts (1.5.1-3) unstable; urgency=low

  * [10595af] Do not break package building, if the autotest failed.

 -- Anton Gladky <gladky.anton@gmail.com>  Sun, 04 Mar 2012 09:44:07 +0100

liggghts (1.5.1-2) unstable; urgency=low

  * [65d77e5] Update copyright-format file due to DEP-5 release.
  * [8fc58b6] Add auto-tests.
  * [0955950] Use Standards-Version: 3.9.3. No changes.
  * [43fb8bf] Use compilation flags proposed by build environment
              instead of hardcoded.
  * [6ed3623] Fix FTBFS "format not a string literal and no format arg".
  * [9d602d4] Enable JPEG. Seems FFTW hurts the productivity.
  * [df4535f] Use dh compat 9.

 -- Anton Gladky <gladky.anton@gmail.com>  Sat, 03 Mar 2012 07:36:10 +0100

liggghts (1.5.1-1) unstable; urgency=low

  * [7e46270] Imported Upstream version 1.5.1
  * [b2f3ee0] Use --parallel option for faster builds.
  * [bdc9cba] Add name of interpretator into headers of some executable
              example scripts.

 -- Anton Gladky <gladky.anton@gmail.com>  Fri, 03 Feb 2012 22:33:06 +0100

liggghts (1.5-1) unstable; urgency=low

  * [9529b4b] Install a manpage.
  * [8d5c6d1] Imported Upstream version 1.5

 -- Anton Gladky <gladky.anton@gmail.com>  Fri, 02 Dec 2011 21:40:31 +0100

liggghts (1.4.6-1) unstable; urgency=low

  * Initial import. (Closes: #647235)

 -- Anton Gladky <gladky.anton@gmail.com>  Fri, 04 Nov 2011 20:43:49 +0100
